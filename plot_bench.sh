#!/bin/sh

PLOTDIR="../ocamlecoevo/plot"

# Plot benchmark results for PRM exact vs approx simulations
python3 $PLOTDIR/bench_exact_approx.py

# Plot benchmark results for approx PRM augmentation
# vs subject level augmentation
python3 $PLOTDIR/bench.py\
  --in _data/benchmark_sir_1234.csv\
  --width 345.\
  --dpi 600.\
  --latex\
  --context paper_cramped\
  --out _data/plot/mcmc/sir/run.1234.mcmc.benchmark.paper_cramped.png


# Plot param estimates for the 2000 hosts population of the benchmark
python3 $PLOTDIR/params.py\
  --read-label "Method"\
  --read "PRM-augmentation" _data/mcmc/sir/run.1234.2.mcmc.111.custom.approx/run.iter.theta.csv\
  --read "Subject-level augmentation" _data/fintzi/run.1234.2.fintzi.20229622.theta.rescaled.csv\
  --target "_data/sir/run.1234.2.par.csv"\
  --parameters beta s0 i0\
  --rename beta "$\beta$"\
  --rename s0 "\$S_0\$"\
  --rename i0 "\$I_0\$"\
  --triangular\
  --alpha 0.1\
  --no-trace\
  --latex\
  --width 345\
  --dpi 600\
  --context paper_cramped\
  --out "_data/plot/mcmc/sir/run.1234.2.mcmc.compare.theta.{}.paper_cramped.png"
