#!/bin/sh

# Simulate 5 years of seasonal data
epi-ode-seirs\
  -init-from _data/seirs/run.5years.ode.cut.par.csv\
  -h 1e-2\
  -out _data/seirs/run.5years.ode

# FIXME also cut the data at 5 years
