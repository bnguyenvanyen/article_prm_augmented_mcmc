#!/bin/sh

# Plot results of MCMC inference for seasonal data diagnosis

PLOTDIR="../ocamlecoevo/plot"
DIR="_data/mcmc/seirs/run.5years.ode.mcmc{}.prm_approx.333"


# cases and prms estimates (main text)
python3 $PLOTDIR/cases_and_prms.py\
  --template $DIR"/run.iter"\
  --columns prior seasonal constant\
  --events I_infection\
  --observations "_data/seirs/run.5years.ode.data.cases.csv"\
  --case-data prior ".beta_constant.prior"\
  --prm-data prior ".beta_constant.prior"\
  --case-data seasonal ""\
  --prm-data seasonal ""\
  --case-data constant ".beta_constant"\
  --prm-data constant ".beta_constant"\
  --rename t "time (years)"\
  --rename prior "constant prior"\
  --rename seasonal "seasonal posterior"\
  --rename constant "constant posterior"\
  --rename I_infection "Infection"\
  --rename-title cases "\textbf{(a)}"\
  --rename-title density "\textbf{(b)}"\
  --rename-title autocorr "\textbf{(c)}"\
  --autocorr\
  --keep-every 4000\
  --ylimits cases 0. 6.\
  --alpha 0.02\
  --latex\
  --width 345\
  --dpi 600\
  --context paper_cramped\
  --out "_data/plot/mcmc/seirs/run.5years.ode.mcmc.333.trio.paper_cramped.png"

# parameter estimates (appendix)
python3 $PLOTDIR/params.py\
  --read-label "Method"\
  --read-template $DIR"/run.iter.theta.csv"\
  --target "_data/seirs/run.5years.ode.par.csv"\
  --read "seasonal posterior" ""\
  --read "constant posterior" ".beta_constant"\
  --parameters loglik beta s0 i0\
  --rename beta "$\beta$"\
  --rename s0 "\$S_0$"\
  --rename i0 "\$I_0$"\
  --alpha 0.1\
  --triangular\
  --width 345.\
  --dpi 600.\
  --context paper_cramped\
  --out "_data/plot/mcmc/seirs/run.5years.ode.mcmc.333.theta.{}.paper_cramped.png"
