#!/bin/sh

# Plot results of MCMC inference for Zika in French Polynesia

PLOTDIR="../ocamlecoevo/plot"
DIR="_data/mcmc/seirs/french_poly_zika_moorea.mcmc.constrained.{}.444"

# Moorea, chain 444, parameter estimates ODE against PRM (appendix)
python3 $PLOTDIR/params.py\
  --read-template $DIR"/run.iter.theta.csv"\
  --read-label "Method"\
  --read "ODE" "ode"\
  --read "PRM" "prm_approx"\
  --parameters loglik beta nu sigma rho\
  --rename beta "$\beta$"\
  --rename nu "$\gamma$"\
  --rename sigma "$\sigma$"\
  --rename rho "$\rho$"\
  --triangular\
  --alpha 0.1\
  --latex\
  --width 345\
  --dpi 600\
  --context paper_cramped\
  --out "_data/plot/mcmc/seirs/french_poly_zika_moorea.mcmc.constrained.theta.{}.paper_cramped.png"


# Moorea, chain 444, case and prms trajs
python3 $PLOTDIR/cases_and_prms.py\
  --observations "_data/french_poly_zika_moorea.data.cases.csv"\
  --template $DIR"/run.iter"\
  --columns mcmc\
  --case-data mcmc "prm_approx"\
  --prm-data mcmc "prm_approx"\
  --events Leave_exposed I_infection Recovery\
  --rename mcmc "estimates"\
  --rename "Leave_exposed" "Leave exposed"\
  --rename "I_infection" "Infection"\
  --keep-every 1000\
  --alpha 0.1\
  --latex\
  --context paper_cramped\
  --width 180\
  --aspect 2.\
  --dpi 600\
  --out "_data/plot/mcmc/seirs/french_poly_zika_moorea.mcmc.constrained.prm_approx.444.cap.paper_cramped.png"
